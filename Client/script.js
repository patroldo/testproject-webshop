var page = 1;

$(document).ready(function() {

	$("button.more").click(LoadMore_Click);
	
	function LoadMore_Click(event) {
		$("div.black_wrap").toggleClass("d-none");
		$.ajax({
			url: "http://localhost:40213/list.php?page=" + (page++) + "&per_page=4",
			method: "GET"
		}).done(AddingTextToHtml);
	}
	LoadMore_Click(null);
});

function MouseOverRowOffer(item) {
	item.classList.add("active")
;}

function MouseOutRowOffer(item) {
	item.classList.remove("active")
}

function AddingTextToHtml(data) {
	data = JSON.parse(data);
	var entities = data.entities;
	$("div.black_wrap").toggleClass("d-none");
	var g = CreateNewRowOffer(entities);
	$(".products .product_list").append(g);
	if(entities.length < 4)
		$(".more").toggleClass("d-none");
}

function CreateNewRowOffer(row_offer) {
	var newRowOffer = "";
	for(var offerIndex = 0; offerIndex < row_offer.length; offerIndex++) {
		var offer = row_offer[offerIndex];
		newRowOffer += "<div class='product animated fadeInUp'>";
		newRowOffer += "<div class='image_wrap'>";
		if(offer.discountCost != null) {
			newRowOffer += "<span class='sale'>sale</span>";
		}
		if(offer.new) {
			newRowOffer += "<span class='new'>new</span>";
		}
		newRowOffer += "<img src='" + offer.img + "' />";
		newRowOffer += "</div>";
		newRowOffer += "<div class='description'>";
		newRowOffer += "<h6 class='description__title'>"+ offer.title +"</h6>";
		newRowOffer += "<p class='description__text'>"+ offer.description +"</p>";
		newRowOffer += "</div>";
		//newRowOffer += "<p class='title'>"+ offer.title +"</p>";
		//newRowOffer += "<p class='description'>"+ offer.description +"</p>";
		newRowOffer += "<div class='price'>";
		if(offer.discountCost != null) {
			newRowOffer += "<span class='price'>$"+ offer.discountCost.toFixed(2) +" </span>";
			newRowOffer += "<span class='old_price'>$"+ offer.cost.toFixed(2) +"</span>";
		}
		else {
			newRowOffer += "<span class='price'>$"+ offer.cost.toFixed(2) +"</span>";
		}
		newRowOffer += "</div>";
		newRowOffer += "<div class='buttons'>";
		newRowOffer += 	"<button class='add_to_card'>Add to card</button>";
		newRowOffer += 	"<button class='view'>View</button>";
		newRowOffer += "</div>";
		newRowOffer += "</div>";
	}
	return newRowOffer;
}
