# My project

This project is a simple template for web-shop without header. Back-end and front-end are in corresponding folders.

The following instruction works on linux (tested on Ubuntu 18.04). The internet must be accessed

## Getting started

Linux instruction to run the project. To start the project you have to:

1. Clone repository
2. Open terminal
3. Enter to cloned repository from terminal
4. Copy the content of folder "Server" to your server to root-location and start it on port 40213. Or do the follow steps to open server on your machine
	1. Enter to folder "Server" 
	2. Execute command
```
$ php -S 0.0.0.0:40213 -t .
```

After this steps your server is ready to send the data. 

1. Enter the folder "Client"
2. Open index.html
3. If you need to test it on other devices in your local network, execute:

```
php -S 0.0.0.0:40212 -t .
```

4. Check the ip of your host
5. Enter on device the ip, that you saw on previous step. For example: "http://192.168.180.158:40212"
